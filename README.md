# README #

Welcome in **VAL**idation **F**luent frame**WORK**

### USAGE ###


```
#!javascript

var ValFWork = require('./valwork.js');

console.log('Validations tester');

var result = new ValFWork('userId', '1234').isInt().minValue(20).maxValue(10).isEmail();

if (!result.isValid) console.log(JSON.stringify(result.errors));
```