var ValFWork = require('./valwork.js');

console.log('Validations tester');

var result = new ValFWork('userId', '1234').isInt().minValue(20).maxValue(10).isEmail();

if (!result.isValid) console.log(JSON.stringify(result.errors));

var someString = 'kamil@k7k.com.pl';

var res = someString.match("/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/");

console.log(JSON.stringify(res));