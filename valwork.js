//ValFWork - VALidation Fluent frameWORK

function ValFWork(name, value) {
    this.name = name;
    this.value = value;
    this.isValid = true;
    this.errors = [];
}

ValFWork.prototype.isInt = function() {
    if (isNaN(this.value)) {
        this.isValid = false;
        this.errors.push(this.name + ' should be an integer.');
    }
    return this;
};

ValFWork.prototype.minValue = function(minValue) {
    if (this.value < minValue) {
        this.isValid = false;
        this.errors.push(this.name + ' should be greater than ' + minValue);
    }
    return this;
};

ValFWork.prototype.maxValue = function(maxValue) {
    if (this.value > maxValue) {
        this.isValid = false;
        this.errors.push(this.name + ' should be less than ' + maxValue);
    }
    return this;
};

ValFWork.prototype.isEmail = function() {
    if (this.value.match("^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$") == null) {
        this.isValid = false;
        this.errors.push(this.name + ' should be an email.');
    }
    return this;
};


module.exports = ValFWork;